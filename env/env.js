let logger = require('../helpers/logger/logger');
logger.debug('Port : ', process.env.CHAT_SERVER_PORT);
var port = process.env.CHAT_SERVER_PORT;
if (port == null || port.length === 0) {
    errorAndExit('CHAT_SERVER_PORT');
}

var env = process.env.CHAT_SERVER_ENV;
logger.debug('CHAT_SERVER_ENV : ', process.env.CHAT_SERVER_ENV);
if (env == null || env.length === 0) {
    errorAndExit('CHAT_SERVER_ENV');
}

var dbUser = process.env.CHAT_SERVER_DB_USER;
logger.debug('CHAT_SERVER_DB_USER : ', process.env.CHAT_SERVER_DB_USER);
if (dbUser == null || dbUser.length === 0) {
    errorAndExit('CHAT_SERVER_DB_USER');
}


var dbPwd = process.env.CHAT_SERVER_DB_PWD;
logger.debug('CHAT_SERVER_DB_PWD : ', process.env.CHAT_SERVER_DB_PWD);
if (dbPwd == null || dbPwd.length === 0) {
    errorAndExit('CHAT_SERVER_DB_PWD');
}


var https = process.env.CHAT_SERVER_HTTPS;
logger.debug('CHAT_SERVER_HTTPS : ', process.env.CHAT_SERVER_HTTPS);
if (https == null || https.length === 0) {
    errorAndExit('CHAT_SERVER_HTTPS');
}




function errorAndExit(envVar) {
    logger.error(envVar + ' environment variable is not set - exiting.');
    process.exit(1);
}

var Env = {
    CHAT_SERVER_PORT: port,
    CHAT_SERVER_ENV: env,
    CHAT_SERVER_DB_USER:dbUser,
    CHAT_SERVER_DB_PWD:dbPwd,
    CHAT_SERVER_HTTPS:https
};

module.exports = Env;
