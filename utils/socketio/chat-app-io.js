let logger = require('../../helpers/logger/logger');

let ChatMessageController = require('../../apps/chat/controllers/chat-message.controller');
let chatMessageController = new ChatMessageController();

module.exports = function (io) {
    //Set socket.io listeners.
    io.on('connection', (socket) => {
        logger.debug('Connected To chat app');

        socket.on('chat-join-private-chat', (params, callback) => {
            logger.debug('Join IndividualChat Room : ', params);
            socket.join(params.roomInfo.room1);
            socket.join(params.roomInfo.room2);
            io.emit('chat-private-chat-joined', params);
            callback();
        });
        socket.on('chat-leave-private-chat', (params, callback) => {
            logger.debug('Leaving IndividualChat Room : ', params);
            socket.leave(params.roomInfo.room1);
            socket.leave(params.roomInfo.room2);
            callback();
        });
        socket.on('chat-send-private-chat-msg', (params) => {
            logger.debug('Send Message to room : ', params);
            chatMessageController.saveMessage(params);
        });

        socket.on('get-all-old-msg', (params, callback) => {
            logger.debug('send message : ', params);
        });

        socket.on('chat-join-group-chat', (params, callback) => {
            logger.debug('Join group chat room : ', params);
            socket.join(params.room);
            io.emit('chat-group-chat-joined', params);
            callback();
        });

        socket.on('chat-leave-group-chat', (params, callback) => {
            logger.debug('Group Join : ', params);
            socket.join(params.room);
            callback();
        });

        socket.on('chat-send-group-msg', (params, callback) => {
            logger.debug('Send Message to group chat room : ', params);
            callback();
        });

        socket.on('chat-create-group-msg', (params) => {
            logger.debug('Send group create message to room : ', params);
        });

        socket.on('chat-get-group-member', (params, callback) => {
            logger.debug('send message : ', params);
        });

        socket.on('chat-remove-group-member', (params, callback) => {
            logger.debug('send message : ', params);
        });

        socket.on('chat-typing', (params, callback) => {
            logger.debug('send message : ', params);
        });

        socket.on('chat-stop-typing', (params, callback) => {
            logger.debug('send message : ', params);
        });


        socket.on('chat-login', (params) => {
            logger.debug('login Join : ', params);
        });

        socket.on('disconnect', () => {
            logger.debug('disconnect');
        });
    });
};
