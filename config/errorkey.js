const ErrorKey = {

    400 :{
        message :"Bad Request"
    },
    401 :{
        message :"Unauthorized"
    },
    402 :{
        message :"Payment Required"
    },
    403 :{
        message :"Forbidden"
    },
    404 :{
        message :"Not Found"
    },
    409 :{
        message :"Conflict"
    },
    2000 :{
        message :"Error While Deleting Temporary file after uploading to s3 bucket"
    },
    2001 :{
        message :"Error uploading file to s3 bucket"
    },
    1100 : {message :"Already Exist"},
    1101 : {message :"Could not save to database"},
    1102 : {message :"Could not save to database"}
};

exports.getErrorObject = function(errorCode){
    return ErrorKey[errorCode];
};
