module.exports = Object.freeze({
    STATUS: {
        OK: 200,
        CREATED: 201
    },
});
