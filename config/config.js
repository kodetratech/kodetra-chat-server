module.exports = {
  env: 'development',
  agendaJob: false,
  envConfig: {
    development: {
      host: 'http://localhost',
      port: '3000'
    },
  }
};
