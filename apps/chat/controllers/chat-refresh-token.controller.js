let logger = require('../../../helpers/logger/logger');
const responseHelper = require('../../../helpers/response-handler/response-handler');
let constants = require('../../../config/constants');
const Boom = require('boom');
const RefreshToken = require('../models/chat-reresh-token.model');


class ChatRefreshTokenController {

    async addRefreshToken(refresh_token, userId, userName) {
        logger.debug("Add user " + JSON.stringify(refresh_token));
        const refreshToken = await RefreshToken.findOne({refresh_token: refresh_token});
        logger.debug('Token Found in database ', refreshToken);
        if (refreshToken !== null) {
            logger.debug('Token Already Exist');
        } else {
            var refreshTokenModel = new RefreshToken();
            refreshTokenModel.refresh_token = refresh_token;
            refreshTokenModel.userId = userId;
            refreshTokenModel.userName = userName;
            return await refreshTokenModel.save();
        }
    };
}

module.exports = ChatRefreshTokenController;

