var logger = require('../../helpers/logger/logger');
var Device = require('../schemas/device.schema');
var responseHelper = require('../../helpers/response-handler/response-handler');
var fieldsSelection = 'deviceId deviceUuid deviceModel devicePlatform deviceManufacturer deviceSerial devicePushToken';
var constants = require('../../config/constants');
var Promise = require('bluebird');
let IOSPushNotification = require('../../utils/push-notification/ios-push-notification');
let pushNotification = new IOSPushNotification();

exports.createDevice = function (req, res) {
    logger.debug("New Device " + JSON.stringify(req.body.device));
    let deviceData = req.body.device;
    Device.findOne({deviceUuid: deviceData.uuid}, function (err, deviceFound) {
        if (err) {
            responseHelper.errorHandler(req, res, err);
        } else {
            if (deviceFound) {
                // Duplicate record
                logger.debug("Device Already Exist");
                deviceFound.devicePushToken = deviceData.pushToken;
                deviceFound.save(function (err, deviceRegistered) {
                    if (err) {
                        logger.debug(JSON.stringify(err));
                        responseHelper.errorHandler(req, res, err);
                    } else {
                        logger.debug('Device updated!! ' + JSON.stringify(deviceRegistered));
                        //pushNotification.sendIosPushNotification(deviceRegistered.devicePushToken);
                        responseHelper.successHandler(req, res, constants.STATUS.OK, deviceRegistered);
                    }
                });
            } else {
                let deviceModel = new Device();
                deviceModel.deviceUuid = deviceData.uuid;
                deviceModel.deviceModel = deviceData.model;
                deviceModel.devicePlatform = deviceData.platform;
                deviceModel.deviceManufacturer = deviceData.manufacturer;
                deviceModel.deviceSerial = deviceData.serial;
                deviceModel.devicePushToken = deviceData.pushToken;
                deviceModel.save(function (err, deviceRegistered) {
                    if (err) {
                        logger.debug(JSON.stringify(err));
                        responseHelper.errorHandler(req, res, err);
                    } else {
                        logger.debug('Device Registered!! ' + JSON.stringify(deviceRegistered));
                        //pushNotification.sendIosPushNotification(deviceRegistered.devicePushToken);
                        responseHelper.successHandler(req, res, constants.STATUS.CREATED, deviceRegistered);
                    }
                });
            }
        }
    });
};

exports.getAllDevices = function (req, res) {
    let page = 1;
    var limit = 10;
    if (req.query.page !== undefined && req.query.page > 0) {
        page = req.query.page;
    }
    if (req.query.limit !== undefined && req.query.limit > 0) {
        page = parseInt(req.query.limit);
    }
    var query = {deviceStatus: 10};
    var options = {
        select: fieldsSelection,
        sort: {deviceName: -1},
        lean: false,
        page: page,
        limit: limit
    };
    Device.paginate(query, options, function (err, devices) {
        if (err) {
            logger.debug("Error occur" + JSON.stringify(err));
            responseHelper.errorHandler(req, res, err);
        } else {
            logger.debug('product found!' + JSON.stringify(devices));
            responseHelper.successHandler(req, res, constants.STATUS.OK, devices);
        }
    });
};

exports.getDeviceTokens = function (req, res) {
    var result;
    var page = 1;
    var limit = 1000;
    // if (req.query.page !== undefined && req.query.page > 0) {
    //     page = req.query.page;
    // }
    // if (req.query.limit !== undefined && req.query.limit > 0) {
    //     page = parseInt(req.query.limit);
    // }
    var query = {};
    var options = {
        select: 'devicePushToken',
        sort: {deviceId: -1},
        lean: false,
        page: page,
        limit: limit
    };
    return new Promise(function (fulfill, reject) {
        Device.paginate(query, options, function (error, response) {
            if (error) {
                logger.debug("Error occur" + JSON.stringify(err));
                reject(error);
            } else {
                logger.debug('product found!' + JSON.stringify(response));
                fulfill(response)
            }
        });
    });
};

exports.getDeviceById = function (req, res) {

};


exports.deleteDeviceById = function (req, res) {

};
exports.updateDeviceeById = function (req, res) {

};

