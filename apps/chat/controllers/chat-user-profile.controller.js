let logger = require('../../../helpers/logger/logger');
const Boom = require('boom');

const ChatUserProfile = require('../models/chat-user-profile.model');

class ChatUserProfileController {

    async createUserProfile(profileData) {
        try {

            let chatUserProfileModel = new ChatUserProfile();
            chatUserProfileModel.user = profileData._id;
            chatUserProfileModel.full_name = profileData.name;
            chatUserProfileModel.role = profileData.role;
            return await chatUserProfileModel.save();
        } catch (error) {
            logger.info('Error While Creating New  User Profile', error);
            const errorResult = new Error('Bad Request');
            errorResult.httpStatusCode = 400;
            throw errorResult;
        }
    }

    async getProfileByUserId(userId) {
        try {
            logger.debug('get Profile By User Id', userId);
            let result = await ChatUserProfile.findOne({user: userId})
                .populate({
                    path: 'user',
                    select: 'name email gender phone country role account address occupation dateOfBirth profilePicLink'
                })
                .populate('groups');
            return result;
        } catch (error) {
            logger.debug('Could Not Get User Profile : ', error);
            const errorResult = new Error('Could Not Get User Profile');
            errorResult.httpStatusCode = 400;
            throw errorResult;
        }
    }

    async getProfiles(userId) {
        try {
            logger.debug('get Profile');
            let result = await ChatUserProfile.find({user: {$ne: userId}})
                .populate({
                    path: 'user',
                    select: 'name email gender phone country role account address occupation dateOfBirth profilePicLink'
                })
                .populate('groups');
            return result;
        } catch (error) {
            logger.debug('Could Not Get User Profile : ', error);
            const errorResult = new Error('Could Not Get User Profile');
            errorResult.httpStatusCode = 400;
            throw errorResult;
        }
    }
}

module.exports = ChatUserProfileController;
