let logger = require('../../../helpers/logger/logger');
const Boom = require('boom');
const uuidv4 = require('uuid/v4');
let Group = require('../models/chat-group.model');
let Message = require('../models/chat-message.model');
let User = require('../models/chat-user.model');
let socketIo = require('../../../utils/socketio/global-socket-io');

let Device = require('../models/chat-device.model');
let mongoose = require('mongoose');

class ChatMessageController {

    async saveMessage(params) {
        logger.debug('message params :', params);
        let messageInfo = params.message;
        logger.debug('message messageInfo :', messageInfo);
        let chatId = null;
        if (messageInfo.conversationId === null) {
            chatId = uuidv4();
            logger.debug('messageInfo.conversationId :', messageInfo.conversationId);
        } else {
            chatId = messageInfo.conversationId
        }
        let recipient = await User.findById(messageInfo.recipientId);
        let message = new Message();
        message.senderId = messageInfo.senderId;
        message.recipientId = messageInfo.recipientId;
        message.sender = messageInfo.senderId;
        message.recipient = messageInfo.recipientId;
        message.messageText = messageInfo.messageText;
        message.conversationId = chatId;
        message.groupId = messageInfo.groupId ? messageInfo.groupId : 0;
        message.readUnreadFlg = recipient && recipient.online ? 0 : 1;
        message.readInfo.push(
            {
                userId: messageInfo.recipientId,
                isRead: recipient && recipient.online,
            }
        );

        const savedMessage = await message.save();
        let updatedMessage = await savedMessage.populate([
            {
                path: 'sender',
                select: 'name profilePicLink phone',
            },
            {
                path: 'recipient',
                select: 'name profilePicLink phone',
            }
        ]).execPopulate();
        logger.debug('New message create :', updatedMessage);
        logger.debug('Send Message to room :', params.roomInfo.room1);
        logger.debug('recipient user:', recipient);
        if (recipient && !recipient.online) {
            let notification = {
                message: messageInfo.messageText
            };
            let message = {
                payload: notification,
                title: 'New Message',
            };
        }
        socketIo.io.in(params.roomInfo.room1).emit('chat-private-group-msg-receive', updatedMessage);
    }

    async getChatMessages(sender, recipient) {
        try {

            const messages = await Message.find({
                $or: [
                    {senderId: sender, recipientId: recipient},
                    {senderId: recipient, recipientId: sender}
                ]
            });
            logger.debug('messages', messages);
            messages.forEach(async (msg) => {
                if (msg.readInfo && msg.readInfo.length > 0) {
                    msg.readInfo.forEach(async (readInfo) => {
                        await Message.update({
                            _id: msg._id,
                            'readInfo._id': readInfo.id
                        }, {$set: {"readInfo.$.isRead": true}}, {
                            upsert: false,
                            new: true
                        });
                    });
                }
            });
            let aggregateResult = await
                Message.aggregate(
                    [
                        {
                            $match: {
                                $or: [
                                    {senderId: sender, recipientId: recipient},
                                    {senderId: recipient, recipientId: sender}
                                ]
                            }
                        },
                        {$lookup: {from: 'chat_users', localField: 'sender', foreignField: '_id', as: 'sender'}},
                        {
                            $lookup: {
                                from: 'chat_users',
                                localField: 'recipient',
                                foreignField: '_id',
                                as: 'recipient'
                            }
                        },

                    ]);
            logger.debug('Loading Previous Chat Messages :', JSON.stringify(aggregateResult));
            return aggregateResult
        } catch (error) {
            logger.debug('Error while loading the message', error);
        }

    }

    async getChatConversations(userId) {
        try {
            return await Message.aggregate([
                {
                    $match: {
                        $or: [
                            {
                                senderId: userId,
                            },
                            {
                                recipientId: userId
                            }
                        ]
                    }
                },
                {
                    $project: {
                        readInfo: {
                            $filter: {
                                input: "$readInfo",
                                as: "item",
                                cond: {
                                    $and: [
                                        {$ne: ["$$item.userId", new mongoose.Types.ObjectId(userId)]},
                                    ]
                                }
                            }
                        },
                        recipient: 1,
                        messageText: 1,
                    }
                },
                {
                    $unwind: "$readInfo"
                },
                {
                    $group: {
                        _id: {
                            "recipient": "$recipient",
                        },
                        "unread": {"$sum": {"$cond": ["$readInfo.isRead", 0, 1]}},
                        "read": {"$sum": {"$cond": ["$readInfo.isRead", 1, 0]}},

                        recipient: {$last: "$recipient"},
                        readInfo: {$last: "$readInfo"},
                        message: {$last: "$messageText"},
                    }
                },
                {
                    $lookup: {
                        from: "chat_user_profiles",
                        localField: "recipient",
                        foreignField: "user",
                        as: "recipient"
                    }
                },
                {
                    $unwind: "$recipient"
                },
                {
                    $lookup: {
                        from: "chat_users",
                        localField: "recipient.user",
                        foreignField: "_id",
                        as: "user"
                    }
                },
                {
                    $unwind: "$user"
                },
                {
                    $project: {
                        "_id": 1,
                        "unread": 1,
                        "read": 1,
                        "readInfo": 1,
                        "message": 1,
                        "recipient.user": 1,
                        "recipient.resume": 1,
                        "resume.full_name": 1,
                        "resume.title": 1,
                        "resume.company": 1,
                        "user.profilePicLink": 1,

                    }
                }
            ])
        } catch (error) {
            logger.debug('Could Not Get chat conversation', error);
            const errorResult = new Error('Could Not Get chat conversation');
            errorResult.httpStatusCode = 400;
            throw errorResult;
        }

        // async getChatConversations(userId) {
        //     try {
        //         return await Message.aggregate([
        //             {
        //                 $match: {
        //                     $or: [
        //                         {sender: new mongoose.Types.ObjectId(userId)},
        //                         {recipient: new mongoose.Types.ObjectId(userId)}
        //                     ]
        //                 }
        //             },
        //             {
        //                 $group: {
        //                     _id: {
        //                         sender: "$sender",
        //                         recipient: "$recipient"
        //                     },
        //                     unread: {
        //                         $sum: "$readUnreadFlg"
        //                     }
        //                 }
        //             },
        //             {
        //                 $project: {
        //                     "_id": 1,
        //                     "sender": "$_id.sender",
        //                     "recipient": "$_id.recipient",
        //                     "unread": 1,
        //                 }
        //             },
        //             {
        //                 $lookup: {
        //                     from: "handshek_users",
        //                     localField: "sender",
        //                     foreignField: "_id",
        //                     as: "sender"
        //                 }
        //             },
        //             {
        //                 $lookup: {
        //                     from: "handshek_users",
        //                     localField: "recipient",
        //                     foreignField: "_id",
        //                     as: "recipient"
        //                 }
        //             },
        //             {
        //                 $unwind: "$sender"
        //             },
        //             {
        //                 $unwind: "$recipient"
        //             },
        //             {
        //                 $project: {
        //                     "_id": 1,
        //                     "unread": 1,
        //                     "sender._id": 1,
        //                     "sender.name": 1,
        //                     "sender.occupation": 1,
        //                     "sender.profilePicLink": 1,
        //                     "sender.role": 1,
        //                     "recipient._id": 1,
        //                     "recipient.name": 1,
        //                     "recipient.occupation": 1,
        //                     "recipient.profilePicLink": 1,
        //                     "recipient.role": 1,
        //                 }
        //             }
        //         ])
        //     } catch (error) {
        //         logger.debug('Could Not Get chat conversation', error);
        //         const errorResult = new Error('Could Not Get chat conversation');
        //         errorResult.httpStatusCode = 400;
        //         throw errorResult;
        //     }
        //
    }
}

module.exports = ChatMessageController;
