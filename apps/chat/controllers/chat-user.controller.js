let logger = require('../../../helpers/logger/logger');
const User = require('../models/chat-user.model');
const crypto = require('crypto');
const fse = require('fs-extra');
let jwt = require('jsonwebtoken');
const Boom = require('boom');
let path = require('path');
let encryptionKeyFile = path.join(__dirname, '../../../../keys/', 'private.key');
const fieldsSelection = 'name phone gender email address occupation credentialId credentialRole credentialToken';
let bcrypt = require('bcrypt');
let SALT_WORK_FACTOR = 10;
// max of 5 attempts, resulting in a 2 hour lock
let MAX_LOGIN_ATTEMPTS = 5;
let LOCK_TIME = 2 * 60 * 60 * 1000;
const uuidv4 = require('uuid/v4');

const ChatRefreshTokenController = require('./chat-refresh-token.controller');
const chatRefreshTokenController = new ChatRefreshTokenController();

const ChatUserProfileController = require('./chat-user-profile.controller');
const chatUserProfileController = new ChatUserProfileController();

class ChatUserController {

    async createUser(userdata) {
        try {
            logger.debug("New User information" + JSON.stringify(userdata));
            let isUserExist = await User.findOne({email: userdata.email});
            logger.debug("is User Exist" + JSON.stringify(isUserExist));
            if (isUserExist) {
                const errorResult = new Error('User Already Exist');
                errorResult.code = 401;
                throw (errorResult);
            } else {
                let username = '';
                if (userdata.email && userdata.email.indexOf('@') > -1) {
                    let email = userdata.email.split('@');
                    username = email[0];
                    let userModel = new User();
                    userModel.email = userdata.email;
                    userModel.password = userdata.password;
                    userModel.userName = username;
                    userModel.name = userdata.name;
                    userModel.displayName = userdata.name;
                    userModel.phone = userdata.phone;
                    userModel.deviceId = userdata.deviceId;
                    userModel.country = userdata.country;
                    userModel.role = userdata.role;
                    userModel.location = {
                        type: 'Point',
                        coordinates: [
                            "-74.0059728",
                            "40.7127753"
                        ]
                    };
                    const newUser = await userModel.save();
                    const userProfile = await chatUserProfileController.createUserProfile(newUser);
                    return newUser;
                } else {
                    throw Boom.boomify(new Error('Failed to create new employer'), {statusCode: 400});
                }
            }
        } catch (error) {
            logger.info('User Business Created  error', error.message);
            throw Boom.boomify(new Error('Failed to create new employer'), {statusCode: 400});
        }

    }

    async login(email, password, deviceId) {
        let user = await User.findOne({email: email});
        // make sure the user exists
        if (!user) {
            throw Error('User Not Found');
        }
        // check if the account is currently locked
        if (user.isLocked) {
            // if we have a previous lock that has expired, restart at 1
            user = await this.incLoginAttempt(user);
        }
        const isMatch = await bcrypt.compare(password, user.password);
        logger.debug('Password mach', isMatch);
        // check if the password was a match
        if (isMatch) {
            // if there's no lock or failed attempts, just return the common
            if (!user.loginAttempts && !user.lockUntil) {
            } else {
                // reset attempts and lock info
                user = await user.update({
                    $set: {loginAttempts: 0},
                    $unset: {lockUntil: 1}
                });
            }

            logger.debug('generate token', user);
            const cert = fse.readFileSync(encryptionKeyFile);
            logger.info('Loaded Certificate : ', cert);
            // create a token
            const token = jwt.sign({id: user._id}, cert, {algorithm: 'RS256', expiresIn: '15m'});
            logger.info('JWT token : ', token);
            if (deviceId) {
                await User.findOneAndUpdate({_id: user._id}, {$set: {deviceId: deviceId}}, {
                    upsert: false,
                    new: true
                });
            }
            const refreshToken = uuidv4();
            await chatRefreshTokenController.addRefreshToken(refreshToken, user._id, user.userName);
            const response = {
                success: true,
                message: 'login successfull',
                token: token,
                user: user.getPublicFields(),
                refresh_token: refreshToken
            };
            logger.info('Final Login response to user: ', response);
            return response;
        } else {
            // password is incorrect, so increment login attempts before responding
            // if we have a previous lock that has expired, restart at 1
            user = await this.incLoginAttempt(user);
            throw Boom.boomify(new Error('Invalid usename or password'), {statusCode: 400});
        }
    }

    updateSocketId(socketInfo) {
        return User.findOneAndUpdate({_id: socketInfo.userId}, {
            $set: {socketId: socketInfo.socketId},
        }, {upsert: false, new: true})
            .exec();
    }

    async incLoginAttempt(user) {
        // if we have a previous lock that has expired, restart at 1
        if (user.lockUntil && user.lockUntil < Date.now()) {
            user = await user.update({
                $set: {loginAttempts: 1},
                $unset: {lockUntil: 1}
            });
        } else {
            // otherwise we're incrementing
            let updates = {$inc: {loginAttempts: 1}};
            // lock the account if we've reached max attempts and it's not locked already
            if (user.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS && !user.isLocked) {
                updates.$set = {lockUntil: Date.now() + LOCK_TIME};
            }
            user = await user.update(updates);
        }
        return user;
    }

    async getAllUser(page, limit) {
        try {
            let query = {status: 10};
            let options = {
                select: fieldsSelection,
                sort: {name: -1},
                lean: false,
                page: page,
                limit: limit
            };
            return await User.paginate(query, options);
        } catch (error) {
            logger.error('Unable to get user', error);
            throw Boom.boomify(new Error('Failed to get users'), {statusCode: 400});
        }

    }
}

module.exports = ChatUserController;
