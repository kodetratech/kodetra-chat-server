const chatAppRoutes = require('express').Router();

let multipartMiddleware = require('connect-multiparty');
let multipart = multipartMiddleware();

let ValidateToken = require('../../../helpers/authentication/validate-request');
let validateRequest = new ValidateToken();

let ChatUserApi = require('../apis/chat-user.api');
const chatUserApi = new ChatUserApi();

let ChatUserProfileApi = require('../apis/chat-user-profile.api');
const chatUserProfileApi = new ChatUserProfileApi();

let ChatMessageApi = require('../apis/chat-message.api');
const chatMessageApi = new ChatMessageApi();


chatAppRoutes.post('/user/create', chatUserApi.createUser);
chatAppRoutes.get('/user', chatUserApi.getAllUser);
chatAppRoutes.post('/user/login', chatUserApi.login);

chatAppRoutes.get('/user/profile/:id', chatUserProfileApi.getProfileByUserId);
chatAppRoutes.get('/user/profiles/:id', chatUserProfileApi.getAllProfiles);
chatAppRoutes.post('/refresh/access-token', validateRequest.refreshJwtAccessToken);

chatAppRoutes.post('/connection/previous/chat', validateRequest.validateToken, chatMessageApi.getChatMessages);
chatAppRoutes.post('/connection/previous/conversation', chatMessageApi.getChatConversations);

module.exports = chatAppRoutes;
