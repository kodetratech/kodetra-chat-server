let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let autoIncrement = require('mongoose-auto-increment');
let uniqueValidator = require('mongoose-unique-validator');
autoIncrement.initialize(mongoose);
let Schema = mongoose.Schema;
let bcrypt = require('bcrypt');
let SALT_WORK_FACTOR = 10;
// max of 5 attempts, resulting in a 2 hour lock
let MAX_LOGIN_ATTEMPTS = 5;
let LOCK_TIME = 2 * 60 * 60 * 1000;

// create a schemas
let chatUserSchema = new Schema({
  displayName: {type: String, default: null},
  name: {type: String, default: null},
  firstName: {type: String, default: null},
  lastName: {type: String, default: null},
  userName: {
    type: String,
    required: [true, 'Your username cannot be blank.'],
    trim: true,
    unique: true, // username must be unique
  },
  email: {type: String, default: null},
  password: {type: String, default: null},
  gender: {type: String, default: null},
  phone: {type: String, default: null},
  profilePicLink: {type: String, default: 'https://do78670y0slnz.cloudfront.net/kodetra/images/avatar/user.png'},
  ipAddresses: [],
  status: {type: Number, default: 10},  // 10: active, 20: disabled, 30: deleted
  dateOfBirth: {type: String, default: null},
  occupation: {type: String, default: null},
  profile: {type: Schema.Types.ObjectId, ref: 'chat_user_profile'},
  deviceId: {type: String, default: null},
  socketId: {type: String, default: null},
  appname: {type: String, default: null},
  apptype: {type: String, default: null},
  role: {type: String, default: 'user'},
  address: {
    street1: {type: String, default: null},
    street2: {type: String, default: null},
    city: {type: String, default: null},
    state: {type: String, default: null},
    postalCode: Number
  },
  isVerified: {type: Boolean, default: false},
  resetPasswordToken: {type: String,},
  resetPasswordExpires: {type: Date, default: Date.now},
  // login attempts
  lastLogin: {type: Date, default: Date.now},
  online: {type: Boolean, default: false},
  loginAttempts: {type: Number, required: true, default: 0},
  lockUntil: {type: Number},
  created_at: {type: Date, default: Date.now},
  updated_at: {type: Date, default: Date.now},
});


chatUserSchema.plugin(uniqueValidator);
chatUserSchema.plugin(mongoosePaginate);
chatUserSchema.plugin(autoIncrement.plugin, {
  model: 'chat_user',
  field: 'chat_user_id',
  startAt: 0,
  incrementBy: 1
});
let reasons = chatUserSchema.statics.failedLogin = {
  NOT_FOUND: 0,
  PASSWORD_INCORRECT: 1,
  MAX_ATTEMPTS: 5
};


chatUserSchema.virtual('isLocked').get(function () {
  // check for a future lockUntil timestamp
  return (this.lockUntil && this.lockUntil > Date.now());
});


// on every save, add the date
chatUserSchema.pre('save', function (next) {
  let user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);
    // hash the password along with our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.password = hash;
      // get the current date
      let currentDate = new Date();
      // change the updated_at field to current date
      user.updated_at = currentDate;
      // if created_at doesn't exist, add to that field
      if (!user.created_at)
        user.created_at = currentDate;
      next();
    });
  });
});


chatUserSchema.methods.comparePassword = function (candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
      logger.debug('password match', isMatch);
    if (err) return callback(err);
    callback(null, isMatch);
  });
};


chatUserSchema.methods.incLoginAttempts = function (callback) {
  // if we have a previous lock that has expired, restart at 1
  if (this.lockUntil && this.lockUntil < Date.now()) {
    return this.update({
      $set: {loginAttempts: 1},
      $unset: {lockUntil: 1}
    }, callback);
  }
  // otherwise we're incrementing
  let updates = {$inc: {loginAttempts: 1}};
  // lock the account if we've reached max attempts and it's not locked already
  if (this.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
    updates.$set = {lockUntil: Date.now() + LOCK_TIME};
  }
  return this.update(updates, callback);
};
chatUserSchema.methods.getPublicFields = function () {
  return {
    _id: this._id,
    userId: this.userId,
    userName: this.userName,
    online: this.online,
    isVerified: this.isVerified,
    address: {
      state: this.state,
      city: this.city,
      street2: this.street2,
      street1: this.street1,
    },
    socketId: this.socketId,
    deviceId: this.deviceId,
    occupation: this.occupation,
    dateOfBirth: this.dateOfBirth,
    status: this.status,
    profilePicLink: this.profilePicLink,
    phone: this.phone,
    gender: this.gender,
    email: this.email,
    role: this.role,
    name: this.name,
    businessName: this.businessName,
    businessCategory: this.businessCategory,
    businessSubcategory: this.businessSubcategory,
    displayName: this.displayName,
    appname: this.appname,
    apptype: this.apptype,
    created_at: this.created_at,
    lastLogin: this.lastLogin,
    updated_at: this.updated_at,
  };
};

chatUserSchema.statics.getAuthenticated = function (username, password, callback) {
  this.findOne({'email': username}, function (err, user) {
    if (err) return callback(err);

      logger.debug('common name ', username);
      logger.debug('common password ', password);
      logger.debug('common common ', user);
    // make sure the common exists
    if (!user) {
      return callback(null, null, reasons.NOT_FOUND);
    }

    // check if the account is currently locked
    if (user.isLocked) {
      // just increment login attempts if account is already locked
      return user.incLoginAttempts(function (err) {
        if (err) return callback(err);
        return callback(null, null, reasons.MAX_ATTEMPTS);
      });
    }
    // test for a matching password
    user.comparePassword(password, function (err, isMatch) {
      if (err) return callback(err);

      // check if the password was a match
      if (isMatch) {
        // if there's no lock or failed attempts, just return the common
        if (!user.loginAttempts && !user.lockUntil) return callback(null, user);
        // reset attempts and lock info
        let updates = {
          $set: {loginAttempts: 0},
          $unset: {lockUntil: 1}
        };
        return user.update(updates, function (err) {
          if (err) return callback(err);
          return callback(null, user);
        });
      }

      // password is incorrect, so increment login attempts before responding
      user.incLoginAttempts(function (err) {
        if (err) return callback(err);
        return callback(null, null, reasons.PASSWORD_INCORRECT);
      });
    });
  });
};
// the schemas is useless so far
// we need to create a model using it
let ChatUserModel = mongoose.model('chat_user', chatUserSchema);

// make this available to our users in our Node applications
module.exports = ChatUserModel;
