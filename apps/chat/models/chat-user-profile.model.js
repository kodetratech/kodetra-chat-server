let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);

let Schema = mongoose.Schema;
// create a schema
let chatUserProfileSchema = new Schema({
    full_name: {type: String, default: null},
    user: {type: Schema.Types.ObjectId, ref: 'chat_user'},
    groups: [{type: Schema.Types.ObjectId, ref: 'chat_group'}],
    settings: [],
    role: {type: String, default: 'user'},
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    modifiedBy: {type: String, default: 'System'}
});

chatUserProfileSchema.plugin(mongoosePaginate);
chatUserProfileSchema.plugin(autoIncrement.plugin, {
    model: 'chat_user_profile',
    field: 'chat_user_profile_id',
    startAt: 10000000,
    incrementBy: 1
});
// on every save, add the dates
chatUserProfileSchema.pre('save', function (next) {
    // get the current date
    let currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;
    next();
});
let ChatUserProfileModel = mongoose.model('chat_user_profile', chatUserProfileSchema);

// make this available to our users in our Node applications
module.exports = ChatUserProfileModel;
