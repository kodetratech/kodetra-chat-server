let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);
let Schema = mongoose.Schema;
// create a schemas
let chatDeviceSchema = new Schema({
    deviceId: {type: Number},
    deviceUuid: {type: String, default: null},
    deviceModel: {type: String, default: null},
    devicePlatform: {type: String, default: null},
    deviceManufacturer: {type: String, default: null},
    deviceSerial: {type: String, default: null},
    devicePushToken: {type: String, default: null},
    deviceStatus: {type: Number, default: 10},  // 10: active, 20: disabled, 30: deleted
    created_at: Date,
    updated_at: Date,
    modifiedBy: {type: String, default: 'System'}
});
chatDeviceSchema.plugin(mongoosePaginate);
chatDeviceSchema.plugin(autoIncrement.plugin, {
    model: 'chat_device',
    field: 'deviceId',
    startAt: 100000,
    incrementBy: 1
});
// on every save, add the date
chatDeviceSchema.pre('save', function (next) {
    // get the current date
    let currentDate = new Date();
    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;
    next();
});
// the schemas is useless so far
// we need to create a model using it
let chatDeviceModel = mongoose.model('chat_device', chatDeviceSchema);

// make this available to our users in our Node applications
module.exports = chatDeviceModel;
