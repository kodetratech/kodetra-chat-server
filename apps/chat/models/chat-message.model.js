const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let mongoosePaginate = require('mongoose-paginate');
let autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);
// Schema defines how namaste messages will be stored in MongoDB
const chatMessageSchema = new Schema({
    senderId: {type: String, default: null, required: true},//senderId
    recipientId: {type: String, default: null, required: true},//receiverId
    sender: {type: Schema.Types.ObjectId, ref: 'chat_user', required: true},//senderId
    recipient: {type: Schema.Types.ObjectId, ref: 'chat_user', required: true},//receiverId
    messageText: {type: String, default: null, required: true},
    groupId: {type: Number, default: 0},
    conversationId: {type: String, default: null},
    img: {type: String, default: null},
    imgFlag: {type: Number, default: 0},
    isImgDownloaded: {type: Number, default: 0},
    imageSize: {type: Number, default: 0},
    readUnreadFlg: {type: Number, default: 0},
    isBlocked: {type: Number, default: 0},
    readInfo: [{
        userId: {type: Schema.Types.ObjectId, ref: 'chat_user'},
        isRead: {type: Boolean, default: false},
        isReadTime: {type: String, default: Date.now},
        isDelivered: {type: Boolean, default: false},
        isDeliveredTime: {type: String, default: Date.now},
    }],
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now}
});
chatMessageSchema.plugin(mongoosePaginate);
chatMessageSchema.plugin(autoIncrement.plugin, {
    model: 'chat_message',
    field: 'chat_message_id',
    startAt: 1000,
    incrementBy: 1
});
// on every save, add the dates
chatMessageSchema.pre('save', function (next) {
    // get the current date
    let currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;
    next();
});
let ChatMessageModel = mongoose.model('chat_message', chatMessageSchema);

// make this available to our users in our Node applications
module.exports = ChatMessageModel;
