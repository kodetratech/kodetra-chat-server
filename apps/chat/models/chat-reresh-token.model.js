var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var autoIncrement = require('mongoose-auto-increment');
var uniqueValidator = require('mongoose-unique-validator');
autoIncrement.initialize(mongoose);
var Schema = mongoose.Schema;

// create a schema
var chatRefreshTokenSchema = new Schema({
    refresh_token: {type: String, default: null},
    userId: {type: String, default: null},
    userName: {type: String, default: null},
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
});
chatRefreshTokenSchema.plugin(uniqueValidator);
chatRefreshTokenSchema.plugin(mongoosePaginate);
chatRefreshTokenSchema.plugin(autoIncrement.plugin, {
    model: 'chat_refresh_token',
    field: 'chat_refresh_token_id',
    startAt: 10000,
    incrementBy: 1
});

// on every save, add the date
chatRefreshTokenSchema.pre('save', function (next) {
    var token = this;
    // get the current date
    var currentDate = new Date();
    // change the updated_at field to current date
    token.updated_at = currentDate;
    // if created_at doesn't exist, add to that field
    if (!token.created_at)
        token.created_at = currentDate;
    next();
});

// the schema is useless so far
// we need to create a model using it
var RefreshTokenModel = mongoose.model('chat_refresh_token', chatRefreshTokenSchema);

// make this available to our users in our Node applications
module.exports = RefreshTokenModel;
