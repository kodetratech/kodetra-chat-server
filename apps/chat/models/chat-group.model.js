let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);
let Schema = mongoose.Schema;
// create a schema
let chatGroupSchema = new Schema({
  groupIcon: {type: String, default: 'https://do78670y0slnz.cloudfront.net/kodetra/images/avatar/user.png'},
  groupName: {type: String, default: null},
  groupDescription: {type: String, default: null},
  groupMembers: [{type: Schema.Types.ObjectId, ref: 'chat_user'}],
  groupCreatedBy: {type: Schema.Types.ObjectId, ref: 'chat_user'},
  isGroup: {type: Boolean, default: false},
  groupStatus: {type: String, default: 'active'},
  created_at: {type: Date, default: Date.now},
  updated_at: {type: Date, default: Date.now},
  modifiedBy: {type: String, default: 'System'}
});
chatGroupSchema.plugin(mongoosePaginate);
chatGroupSchema.plugin(autoIncrement.plugin, {
  model: 'chat_group',
  field: 'chat_group_id',
  startAt: 10000000,
  incrementBy: 1
});
// on every save, add the dates
chatGroupSchema.pre('save', function (next) {
  // get the current date
  let currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;
  next();
});
let ChatGroupModel = mongoose.model('chat_group', chatGroupSchema);

// make this available to our users in our Node applications
module.exports = ChatGroupModel;
