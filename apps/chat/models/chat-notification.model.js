let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);
let Schema = mongoose.Schema;
// create a schemas
let chatNotificationSchema = new Schema({
  message: {type: String, default: null},
  type: {type: String, default: null},
  sender: {type: Schema.Types.ObjectId, ref: 'chat_user'},
  recipient: {type: Schema.Types.ObjectId, ref: 'chat_user'},
  status: {type: String, default: 'pending'},
  created_at: {type: Date, default: Date.now},
  updated_at: {type: Date, default: Date.now},
  modifiedBy: {type: String, default: 'System'}

});

chatNotificationSchema.plugin(mongoosePaginate);
chatNotificationSchema.plugin(autoIncrement.plugin, {
  model: 'chat_notification',
  field: 'chat_notification_id',
  startAt: 10000000,
  incrementBy: 1
});
// on every save, add the dates
chatNotificationSchema.pre('save', function (next) {
  // get the current date
  let currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;
  next();
});

let ChatNotificationModel = mongoose.model('chat_notification', chatNotificationSchema);

// make this available to our users in our Node applications
module.exports = ChatNotificationModel;
