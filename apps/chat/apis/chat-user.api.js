let logger = require('../../../helpers/logger/logger');
let constants = require('../../../config/constants');
let responseHelper = require('../../../helpers/response-handler/response-handler');
const Boom = require('boom');
const ChatUserController = require('../controllers/chat-user.controller');
const chatUserController = new ChatUserController();

class ChatUserApi {
    async createUser(req, res, next) {
        try {
            logger.debug("Create New User" + JSON.stringify(req.body));
            let userdata = req.body;
            let newUser = await chatUserController.createUser(userdata);
            responseHelper.successHandler(req, res, constants.STATUS.OK, newUser);
        } catch (error) {
            logger.error('User Registration failed', error.code);
            return next(Boom.badRequest(error))
        }
    }

    async login(req, res, next) {
        logger.error('Login request', req.body);
        try {
            let loginResponse = await chatUserController.login(req.body.email, req.body.password, req.body.deviceId);
            logger.debug("Login response : ", loginResponse);
            responseHelper.successHandler(req, res, constants.STATUS.OK, loginResponse);
        } catch (error) {
            logger.error('Invalid UserName or Password', error);
            return next(Boom.badRequest(error))
        }
    }

    async getAllUser(req, res, next) {
        try {
            let page = 1;
            let limit = 10;
            if (req.query.page !== undefined && req.query.page > 0) {
                page = req.query.page;
            }
            if (req.query.limit !== undefined && req.query.limit > 0) {
                limit = parseInt(req.query.limit);
            }
            const result = await chatUserController.getAllUser(page, limit);
            responseHelper.successHandler(req, res, constants.STATUS.OK, result);
        } catch (error) {
            logger.error('Invalid UserName or Password', error);
            return next(Boom.badRequest(error))
        }
    }
}

module.exports = ChatUserApi;
