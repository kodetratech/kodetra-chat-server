let logger = require('../../../helpers/logger/logger');
const Boom = require('boom');
let constants = require('../../../config/constants');
let responseHelper = require('../../../helpers/response-handler/response-handler');
let ChatMessageController = require('../controllers/chat-message.controller');
const chatMessageController = new ChatMessageController();

class ChatMessageApi {

    async getChatMessages(req, res, next) {
        try {
            const sender = req.body.sender;
            const recipient = req.body.recipient;
            logger.debug("sender Id : " + sender);
            logger.debug("recipient Id : " + recipient);
            let previousChatMessages = await chatMessageController.getChatMessages(sender, recipient);
            responseHelper.successHandler(req, res, constants.STATUS.OK, previousChatMessages);
        } catch (error) {
            logger.error('Not Able to Create New Card', error);
            return next(boom.badRequest('Not Able to Create New Card'))
        }

    }

    async getChatConversations(req, res, next) {
        try {
            const sender = req.body.sender;
            logger.debug("sender Id : " + sender);
            let previousChatConversation = await chatMessageController.getChatConversations(sender);
            responseHelper.successHandler(req, res, constants.STATUS.OK, previousChatConversation);
        } catch (error) {
            logger.error('Not Able to get chat conversation', error);
            return next(boom.badRequest('Not Able to get chat conversation'))
        }
    }
}

module.exports = ChatMessageApi;
