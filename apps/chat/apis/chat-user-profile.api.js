let logger = require('../../../helpers/logger/logger');
let constants = require('../../../config/constants');
let responseHelper = require('../../../helpers/response-handler/response-handler');
const Boom = require('boom');

let ChatUserProfileController = require('../controllers/chat-user-profile.controller');
const chatUserProfileController = new ChatUserProfileController();

class ChatUserProfileApi {

    async getProfileByUserId(req, res, next) {
        try {
            logger.debug("get Profile By User Id : " + JSON.stringify(req.params.id));
            let user = req.params.id;
            let userProfile = await chatUserProfileController.getProfileByUserId(user);
            responseHelper.successHandler(req, res, constants.STATUS.OK, userProfile);
        } catch (error) {
            logger.error('Not Able to Create New Card', error);
            return next(Boom.badRequest('Not Able to Create New Card'))
        }
    }

    async getAllProfiles(req, res, next) {
        try {
            logger.debug("get Profile By User Id : " + JSON.stringify(req.params.id));
            let user = req.params.id;
            let userProfile = await chatUserProfileController.getProfiles(user);
            responseHelper.successHandler(req, res, constants.STATUS.OK, userProfile);
        } catch (error) {
            logger.error('Not Able to Create New Card', error);
            return next(Boom.badRequest('Not Able to Create New Card'))
        }
    }
}

module.exports = ChatUserProfileApi;
