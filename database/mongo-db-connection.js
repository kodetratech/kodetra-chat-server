'use strict';
let env = require('../env/env');
let logger = require('../helpers/logger/logger');
let mongoose = require('mongoose');
mongoose.promise = require('bluebird');
let connectionURL = `mongodb://apidbadmin:${encodeURIComponent(env.CHAT_SERVER_DB_PWD)}@localhost:27017/apidb`;
let options = {
    native_parser: false,
    autoIndex: true,
    poolSize: 5,
    keepAlive: 300000,
    connectTimeoutMS: 30000,
    reconnectTries: 30,
    useNewUrlParser: true,
    promiseLibrary: global.Promise
};

//attach lister to connected event
mongoose.connection.once('connected', function () {
    logger.info("Connected to database")
});

mongoose.connect(connectionURL, options).then(
    (response) => {
        logger.info('Successfully connected to MongoDB');
    },
    err => {
        logger.info('Failed to connect MongoDB ' + err);
    }
);

