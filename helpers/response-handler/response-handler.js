let logger = require('../logger/logger');
let errorCodes = require('../../config/errorkey');
exports.errorHandler = function (req, res, err) {
    let finalErrorResponse = {
        statusCode: '',
        message: '',
        success: false
    };
    if (err.code && err.message) {
        finalErrorResponse.statusCode = err.code;
        finalErrorResponse.message = err.message;
    } else {
        err.code = 400;
        let error = errorCodes.getErrorObject(err.code);
        finalErrorResponse.statusCode = err.code;
        finalErrorResponse.message = error.message;
    }
    logger.debug("Final Error Response ", JSON.stringify(finalErrorResponse));
    res.type('json').status(err.code).json(finalErrorResponse);
};

exports.successHandler = function (req, res, statusCode, data) {
    logger.debug("Success Handler");
    let finalResponse = {
        data: {},
        success: true
    };
    if (data.docs) {
        finalResponse.data = data.docs;
        res.setHeader('X-Total-Docs', data.total);
        res.setHeader('X-Limit', data.limit);
        res.setHeader('X-Page', data.page);
        res.setHeader('X-Pages', data.pages);
    } else {
        finalResponse.data = data;
    }
    logger.debug("Final Response " + JSON.stringify(finalResponse));
    res.type('json').status(statusCode).json(finalResponse);
};


