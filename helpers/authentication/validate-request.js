/**
 * Created by Kodetra Technologies on 4/1/16.
 */
const boom = require('boom');
let logger = require('../logger/logger');
const fse = require('fs-extra');
let jwt = require('jsonwebtoken');
let util = require('util');
let path = require('path');
const RefreshToken = require('../../apps/chat/models/chat-reresh-token.model');
const RefreshTokenController = require('../../apps/chat/controllers/chat-refresh-token.controller');
const refreshTokenCtrl = new RefreshTokenController();
let decryptionKeyFile = path.join(__dirname, '../../../keys/', 'public.pem');
let encryptionKeyFile = path.join(__dirname, '../../../keys/', 'private.key');
const uuidv4 = require('uuid/v4');

class ValidateToken {
    /**
     * This function will token from the header and validate it
     * @param req
     * @param res
     * @param next
     */
    validateToken(req, res, next) {
        let token;
        logger.debug('Headers : ', req.headers);
        // check header or url parameters or post parameters for token
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            token = req.headers.authorization.split(' ')[1];
            // decode token
            if (token) {
                let cert = fse.readFileSync(decryptionKeyFile);
                let jwtVerify = util.promisify(jwt.verify);
                jwtVerify(token, cert, {algorithms: ['RS256']}).then((response) => {
                    logger.debug('Decoded token : ', response);
                    next();
                }).catch((error) => {
                    logger.debug('Jwt token eroor', error);
                    return res.status(401).send({
                        success: false,
                        message: 'Failed to authenticate token.'
                    });
                });
            } else {
                // if there is no token
                return res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });
            }
        } else {
            return res.status(403).send({
                success: false,
                message: 'Failed to authenticate token.'
            });
        }
    };

    verifyJwtAccessToken(req, res, next) {
        let token = req.body.token;
        logger.debug('Headers : ', req.headers);
        if (token) {
            let cert = fse.readFileSync(decryptionKeyFile);
            let jwtVerify = util.promisify(jwt.verify);
            jwtVerify(token, cert, {algorithms: ['RS256']}).then((response) => {
                logger.debug('Decoded token : ', response);
                next();
            }).catch((error) => {
                logger.debug('Jwt token eroor', error);
                return res.status(400).send({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            });
        } else {
            // if there is no token
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    };

    async refreshJwtAccessToken(req, res, next) {
        let userId = req.body.id;
        let refreshToken = req.body.refresh_token;
        logger.debug('Headers ********* : ', req.headers);
        logger.debug('userId ************ : ', userId);
        logger.debug('userIdrefreshToken ************ : ', refreshToken);
        const result = await RefreshToken.findOne({refresh_token: refreshToken});
        logger.debug('result refresh token ************ : ', result);
        logger.debug('result refresh null ************ : ', result !== null);
        logger.debug('result refresh result.userId ************ : ', result.userId === userId);
        logger.debug('result refresh esult.refresh_token ************ : ', result.refresh_token === refreshToken);
        if (result && result !== null && result.refresh_token === refreshToken && result.userId === userId) {
            logger.debug('result new userId ************ : ', userId);
            if (userId) {
                logger.debug('result new inside if ************ : ', userId);
                let cert = fse.readFileSync(encryptionKeyFile);
                const token = jwt.sign({id: userId}, cert, {algorithm: 'RS256', expiresIn: '15m'});
                logger.debug('result new token ************ : ', token);
                const newRefreshToken = uuidv4();
                refreshTokenCtrl.addRefreshToken(newRefreshToken, userId, result.userName)
                    .then((result) => {
                        const response = {
                            success: true,
                            message: 'new access token',
                            token: token,
                            refresh_token: newRefreshToken
                        };
                        return res.status(200).send(response);
                    })
                    .catch((error) => {
                        return next(boom.badRequest('Authentication failed. Problem with refresh Token.'));
                    });
            } else {
                // if there is no token
                return res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });
            }
        } else {
            return next(boom.badRequest('Authentication Failed. Invalid User'));
        }

    };
}

module.exports = ValidateToken;
