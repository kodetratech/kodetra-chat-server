let sysEnv = require('../../env/env'),
    bunyan = require('bunyan'),
    level = sysEnv.CHAT_SERVER_ENV !== 'production' ? 'debug' : 'info';

module.exports = bunyan.createLogger({
    name: 'kodetra-chat-server',
    level: level
});
