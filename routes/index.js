const routes = require('express').Router();
const routesChatApp = require('../apps/chat/routes/chat.route');
routes.get('/health', function (req, res, next) {
    return res.type('json').status(200).json({
        message: "Server is healthy",
        success: true
    });
});
routes.use('/api/v1/chat', routesChatApp);
module.exports = routes;
