# Kodetra Chat Server

### Project Setup
1. Clone the Source code : `git clone https://kodetratech@bitbucket.org/kodetratech/kodetra-chat-server.git`
2. `npm install`
3. Create folder "keys/ssl" in the same parent folder where you have cloned the source code. **Not inside the source code folder**
4. Go to keys/ssl folder and generate self signed certificate to run the server as https server using below commands
   * `openssl req -new -newkey rsa:2048 -nodes -keyout chat_server.key -out chat_server.csr`
   * `openssl x509 -req -days 365 -in chat_server.csr -signkey chat_server.key -out chat_server.crt`
5. Go to "Keys" Folder and create private and public keys for jwt token using below commands
   * `openssl genrsa -out private.key 2048`
   * `openssl rsa -in private.key -pubout -out public.pem`
6. Now Go to Kodetra-chat-server/env/env.txt and change the local monogo db credentials eg CHAT_SERVER_DB_USER="apidbadmin" and CHAT_SERVER_DB_PWD="testpwd"
7. Now open terminal and at the root level of the project, run the command:
   * `npm start | bunyan`

### Project and Source Code Structure
* XYZ : Any Folder/Directory under which you want to clone the project
  * Kodetra-chat-server : Project Source Code Root Folder
    * apps :
      * chat 
        * apis : This folder contains the apis files, which will take the client request and send the response back to client.
        * controllers : These folder contains the business logic and read and write to database
        * models : These folder contains the mongodb schemas and models
        * route : This folder has routes and apis mapping
    * config : Any kind of configuration or constant can be place here
    * database : It has file to create mongodb database connection using mongoose
    * env : It has file to declare the enviornment variables
    * helpers : This folder has logger and response utility methods
    * node_modules : npm packages 
    * routes : top level router file
    * utils : any other kind of utility can go here
    * app.js: Entry point of the application
    * package.json : List of npm packages and project description
    * readme.md : Project setup guide and commands
  * Keys
    * privaye.key : private key file
    * public.pem : public key file
    * ssl : Directory
      * chat_server.crt : ssl certificate file
      * chat_server.key : ssl certificate key file
  
### Commands List  
##### Generate Self Signed Certificate for Https Server
 * openssl req -new -newkey rsa:2048 -nodes -keyout chat_server.key -out chat_server.csr
 * openssl x509 -req -days 365 -in chat_server.csr -signkey chat_server.key -out chat_server.crt

##### Generate Private and Public Key for JWT Token
 * openssl genrsa -out private.key 2048
 * openssl rsa -in private.key -pubout -out public.pem


### License : MIT
