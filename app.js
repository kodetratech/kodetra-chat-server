const env = require('./env/env');
const express = require('express');
const path = require('path');
const session = require('express-session');
const flash = require('connect-flash');
const favicon = require('serve-favicon');
const logger = require('./helpers/logger/logger');
const bunyanMiddleware = require('bunyan-middleware');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const device = require('express-device');
const mongodb = require('./database/mongo-db-connection');
const helmet = require('helmet');
const debug = require('debug')('chatserver:server');
const routes = require('./routes');
const port = normalizePort(env.CHAT_SERVER_PORT || '3000'); //Get port from environment and store in Express.
const cors = require('cors');
const csp = require('helmet-csp');
const RateLimit = require('express-rate-limit');
const fse = require('fs-extra');

const globalSocketIo = require('./utils/socketio/global-socket-io');
const ChatAppIO = require('./utils/socketio/chat-app-io');

const encryptionKeyFile = path.join(__dirname, '../keys/ssl', 'chat_server.key');
logger.debug('encryptionKeyFile path: ', encryptionKeyFile);
const sslCertFile = path.join(__dirname, '../keys/ssl', 'chat_server.crt');
const privateKey = fse.readFileSync(encryptionKeyFile);
const sslCert = fse.readFileSync(sslCertFile);
logger.debug('Https is on: ', env.CHAT_SERVER_HTTPS);
const https = env.CHAT_SERVER_HTTPS ? require('https') : require('http');

const app = express();
app.set('port', port);
app.use(helmet());
app.use(csp({
    // Specify directives as normal.
    directives: {
        defaultSrc: ["'self'"],
        scriptSrc: ["'self'"],
        styleSrc: ["'self'"],
        imgSrc: ["'self'"],
        connectSrc: ["'self'"],
        fontSrc: ["'self'"],
        objectSrc: ["'none'"],
        mediaSrc: ["'self'"],
        frameSrc: ["'none'"]
    },

    // This module will detect common mistakes in your directives and throw errors
    // if it finds any. To disable this, enable "loose mode".
    loose: false,

    // Set to true if you only want browsers to report errors, not block them.
    // You may also set this to a function(req, res) in order to decide dynamically
    // whether to use reportOnly mode, e.g., to allow for a dynamic kill switch.
    reportOnly: false,

    // Set to true if you want to blindly set all headers: Content-Security-Policy,
    // X-WebKit-CSP, and X-Content-Security-Policy.
    setAllHeaders: false,

    // Set to true if you want to disable CSP on Android where it can be buggy.
    disableAndroid: false,

    // Set to false if you want to completely disable any user-agent sniffing.
    // This may make the headers less compatible but it will be much faster.
    // This defaults to `true`.
    browserSniff: true
}));
// Sets "Referrer-Policy: same-origin".
app.use(helmet.referrerPolicy({policy: 'same-origin'}));
app.use(helmet.noCache());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.noSniff());
app.use(helmet.hsts({
    maxAge: 31536000
}));
app.set('views', path.join(__dirname, 'views')); // view engine setup
app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//apps.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// set up logger
app.log = logger;
app.use(cookieParser(env.CHAT_SERVER_DB_PWD));
app.use(bunyanMiddleware({requestStart: false, logger: logger}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
app.use(device.capture());
const hour = 3600000 * 24;
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
//only if you're behind a reverse proxy (Heroku, Bluemix, AWS if you use an ELB, custom Nginx setup, etc)
app.enable('trust proxy');
app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // res.header('Access-Control-Allow-Credentials', false);
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Origin,x-xsrf-token,X-Requested-With,content-type,Accept,X-Access-Token,Authorization,X-Key,apptype,appname');
    res.header('Access-Control-Expose-Headers', 'X-Total-Docs,X-Limit,X-Page,X-Pages');
    res.header('Access-Control-Allow-Credentials', 'true');

    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

const apiLimiter = new RateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100,
    delayMs: 0 // disabled
});
app.use('/api/', apiLimiter);

/**
 * Create HTTP server.
 */
const httpsOption = {
    cert: sslCert,
    key: privateKey
};
const server = env.CHAT_SERVER_HTTPS ? https.createServer(httpsOption, app) : https.createServer(app);
server.timeout = 120000;
const io = require('socket.io')(server);
globalSocketIo.io = io;

ChatAppIO(io);
app.use(function (req, res, next) {
    req.io = io;
    next();
});
// const server = https.createServer(app);
/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    logger.info('Listening ' + bind);
    debug('Listening on ' + bind);
}

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        console.log('Dev : Error error', err);
        res.status(err.status || 500);
        res.type('json').json({
            errorCode: err.status,
            message: err.message,
            success: false
        });
        // res.send(err.status + ' ' + err.message);
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.type('json').json({
        errorCode: err.status,
        message: err.message,
        success: false
    });
    // res.send(err.status + ' ' + err.message);
});


module.exports = app;
